/*

OK HOWABOUT SOME RANGE SLIDERSh

ok this is going to iterate through frames of video
there are 3 canvases.
the video is offscreen
video frame gets loaded into c1,
video advances
video frame gets loaded into c2,
difference of each pixel's distance from value of prev frame is calculated

c3, fullscreen, shows corresponding pixels weighted over time -- so that over time pixels that don't change stay bright and ones that do go dark.
ideally weigh so that colors that start light stay light.  

also... a clamped array with 1 channel per pixel that iterates down based on how much it has changed toward zero.

---
ok let's make this better

there are n+1 canvases
the +1 is the sum
we now need objects that can be spawned off prototype
each has a canvas dom element
each has a context
and each has one of those nifty clamped arrays

so like n = 5   [] (interval) [] (interval) []  (interval) [] (interval) [] => sumcanvas

initial: 227 33 67

3 count:
interval (fps): 1469 low: 27.000 high: 173.000 



take a look at
https://stackoverflow.com/questions/19605894/how-can-we-adjust-levels-in-html5-canvas

*/


var $ = function(x) {
    return document.querySelector(x);
}
var highRange = document.querySelector("#high");
var lowRange = document.querySelector("#low");
var interval = document.querySelector("#interval");
var under = document.querySelector("#under");
if (window.location.href.includes("localhost")) {
    var imagerUrl = "http://localhost:3000";
} else {
    var imagerUrl = "https://oversightmachin.es:3000/";
}
highRange.addEventListener("change", function(e) {
    apparatus.hi = parseFloat(e.target.value).toFixed(3);
    document.querySelector("#hival").textContent = apparatus.hi;
});

lowRange.addEventListener("change", function(e) {
    apparatus.lo = parseFloat(e.target.value).toFixed(3);
    document.querySelector("#loval").textContent = apparatus.lo;
});

interval.addEventListener("change", function(e) {
    apparatus.interval = e.target.value
    document.querySelector("#intval").textContent = apparatus.interval;
});

var Difference = function() {
    try {
        this.id = document.querySelectorAll(".difference").length;
    } catch {
        this.id = 0;
    }
    let can = document.createElement("canvas");
    can.width = vid.videoWidth;
    can.height = vid.videoHeight;
    can.classList.add("difference");
    document.querySelector("#container").appendChild(can);
    this.canvas = can;
    this.ctx = can.getContext("2d");
    this.imgD = "";
    this.timepos = 0;
    this.centerPix = 0; //pixel data of center pixel
    this.loadFrame();
}

Difference.prototype.loadFrame = async function() {
    await copyPix(vid, this.ctx);
    this.timepos = vid.currentTime;

    this.centerPix = this.ctx.getImageData(this.canvas.width / 2, this.canvas.height / 2, 1, 1).data;
}

var apparatus = {
    gap: 125
};

apparatus.setup = async function() {
    //this.interval = 5 / 24; // 5 frame interval, no 1 sec
    this.diffs = [];
    this.count = diffs || 4 + getRandomInt(-1, 1);
    this.highest = 255;
    for (let i = 0; i < this.count; i++) {
        this.diffs.push(new Difference());
    }

    this._low(inputLow || 53 + getRandomInt(-11, 11))

    this._high(inputHigh || 155 + getRandomInt(-11, 11));

    this._interval(inputInterval || 102 + getRandomInt(-25, 25));

    let aspect = this.metadata.imageWidth + "/" + this.metadata.imageHeight;

    if (this.metadata.filename) {
        this.recordId = this.metadata.filename;
    }
    if (interval.value > 50) {
        this.gap = 5000;
    } else {
        this.gap = 150;
    }


    this.display = document.querySelector("#display");
    this.display.width = vid.videoWidth;
    this.display.height = vid.videoHeight;
    this.dispCtx = this.display.getContext("2d");
    this.dispCtx.fillStyle = "orange";
    this.dispCtx.fillStyle = "rgba(255,255,255,0)";
    this.dispCtx.strokeStyle = "green";
    this.dispCtx.strokeWidth = "3";

    this.pixelMap = new Uint8ClampedArray(vid.videoWidth * vid.videoHeight);
    this.title = this.metadata.filename.replace(".mp4", "");
    await this.the_title(this.title);
    await this.the_JSON(this.metadata.metadata);
    //under.play();
    await this.cycle();
};

apparatus.pickFootage = async function() {
    let data = await fetch("https://oversightmachin.es/baltimore/data.json");
    data = await data.json();
    console.log("MATCHING", footageid)
    if (footageid) {
        for (let d of data) {
            if (d.filename.replace(".mp4", "").toLowerCase() === footageid.toLowerCase()) {
                console.log("found", d);
                return d;
            }

        }
        console.log("no data found for", d);
    } else {
        return data[Math.floor(Math.random() * data.length)];
    }

}

apparatus.the_title = async function(inc) {
    let title = document.querySelector("#title");
    document.querySelector("title").textContent = "unburning " + inc;
    this.recordId = inc;
    title.textContent = "unburning " + inc;
    title.style.display = "block";
    //await sleep(4242);
    title.style.display = "none";
    return Promise.resolve();
}

apparatus.the_JSON = async function(inc) {
    let jsholder = document.querySelector("#json");

    jsholder.textContent = JSON.stringify(inc);
    jsholder.style.display = "block";

    //await sleep(9001);
    jsholder.style.display = "none";
    return Promise.resolve();
}

var vid = document.querySelector("#srcvideo");

var startTime = 0;

var parsedUrl = new URL(window.location.href);
var whichVid = parsedUrl.searchParams.get("form");
var footageid = parsedUrl.searchParams.get("id");
var inputLow = parsedUrl.searchParams.get("low");
var inputHigh = parsedUrl.searchParams.get("high");
var diffs = parsedUrl.searchParams.get("diffs");

var inputInterval = parsedUrl.searchParams.get("interval");

var record = false;
//setkeystroke to activate that ^^

Mousetrap.bind('r', function() {
    record = !record;
    if (record) {
        apparatus.recordId = apparatus.metadata.metadata.filename;
        document.querySelector("#record").style.display = "block";
        //document.querySelector("#record").textContent = "&bull; " + apparatus.recordId
    } else {
        document.querySelector("#record").style.display = "none";
        apparatus.doneRecording();
    }
});

apparatus._low = function(input) {
    if (!input) {
        return this.lo;
    } else {
        this.lo = input;
        lowRange.value = input;
        document.querySelector("#loval").textContent = apparatus.lo;

    }

}

apparatus._high = function(input) {
    if (!input) {
        return this.hi;
    } else {
        this.hi = input;
        highRange.value = input;
        document.querySelector("#hival").textContent = apparatus.hi;

    }
}

apparatus._interval = function(input) {
    if (!input) {
        return this.in;
    } else {
        this.interval = input;
        interval.value = input;
        document.querySelector("#intval").textContent = apparatus.interval;

    }
}

apparatus.doneRecording = async function() {
    let form = {
        "id": this.recordId,
        "done": true
    };

    var sData = JSON.stringify(form);
    console.log("sending finished");
    try {
        let response = await fetch(imagerUrl, {
            method: "post",
            body: sData
        });
        let r = await response.json();
        console.log("Request succeeded with JSON response", r);
    } catch (e) {
        console.log("fetch catch backup", e);
    }
}

var advanceFrame = async function(time) {
    console.log(time);
    time = parseFloat((time / 25).toFixed(3));
    console.log(time);
    let tar = parseFloat(vid.currentTime) + time;
    console.log(vid.currentTime, tar);
    return new Promise(function(resolve) {
        vid.addEventListener("seeked", resolve, {
            once: true
        });
        if (tar > vid.duration) {
            vid.currentTime = Math.abs(tar - vid.duration);
        } else {
            try {
                //console.log("seeking to " + tar)
                vid.currentTime = tar;
            } catch (e) {
                console.log(tar);
                throw (e);
            }
        }
    });
};

var pixelMap;

var diff = function(c1, c2) {
    let frame1 = c1.getImageData(0, 0, c1.canvas.width, c1.canvas.width);
    let frame2 = c2.getImageData(0, 0, c2.canvas.width, c2.canvas.width);


    let center = c2.getImageData(c2.canvas.width / 2, c2.canvas.height / 2, 1, 1);
    console.log(center.data);
    //document.body.style.background = "rgba(" + Math.abs(255 - center.data[0]) + "," + Math.abs(255 - center.data[1]) + "," + Math.abs(255 - center.data[2]) + ")";

    let l = frame1.data.length / 4;

    for (let i = 0; i < l; i++) {
        let r1 = frame1.data[i * 4 + 0];
        let r2 = frame2.data[i * 4 + 0];
        let g1 = frame1.data[i * 4 + 1];
        let g2 = frame2.data[i * 4 + 1];
        let b1 = frame1.data[i * 4 + 2];
        let b2 = frame2.data[i * 4 + 2];
        let dist = (Math.abs(r2 - r1) + Math.abs(g2 - g1) + Math.abs(b2 - b1));
        //#0ce312

        if (dist > this.hi) {
            //pixelMap[i] = pixelMap[i] - (dist / 2);
            pixelMap[i] = 0;
        }
        if (dist < this.lo) {
            pixelMap[i] = 255; //ugh
        }

        if ((g1 + g2 / 2 >= brighty) && (g1 - r1 > 90 && g1 - b1 > 90) || (g2 - r2 > 90 && g2 - b2 > 90)) {
            pixelMap[i] = 255;
        } else if (((r1 + g1 + b1) / 3 < 200) && ((r2 + g2 + b2) / 3 < 200)) {
            pixelMap[i] = 0;
        }
        //#9da2a0

        if (r1 + r2 + g1 + g2 + b1 + b2 < 20) {
            pixemMap[i] = 0;
        }

        frame1.data[i * 4 + 3] = pixelMap[i];

        if (dist > this.highest) {
            highRange.setAttribute("max", parseInt(dist, 10));
        }
    }

    c3ctx.putImageData(frame1, 0, 0);
};

//[1,2,5,255] -> [1,3,250]
function arrayDiff(A) {
    return A.slice(1).map(function(n, i) {
        return Math.abs(n - A[i]);
    });
}

apparatus.calculate = function() {
    //hard set for now
    let mode = "largest";

    var last = this.diffs[this.diffs.length - 1];
    let l = this.diffs[0].imgD.data.length / 4;
    this.pxtot = 0;
    for (let i = 0; i < l; i++) {
        let px = this.pixelMap[i];

        var difference = 0;
        //r,g,b
        var ref = [this.diffs[0].imgD.data[i * 4], this.diffs[0].imgD.data[i * 4 + 1], this.diffs[0].imgD.data[i * 4 + 2]];

        if (mode === "diff") {
            for (let d of this.diffs) {

                difference += Math.abs(ref[0] - d.imgD.data[i * 4 + 0]);
                difference += Math.abs(ref[1] - d.imgD.data[i * 4 + 1]);
                difference += Math.abs(ref[2] - d.imgD.data[i * 4 + 2]);
                ref = [d.imgD.data[i * 4], d.imgD.data[i * 4 + 1], d.imgD.data[i * 4 + 2]];

            }
            if (difference < this.lo) {
                px = 255;
            } else if (difference > this.hi) {
                px = 255 - difference;
            }
            last.imgD.data[i * 4 + 3] = px;
        } else if (mode === "largest") {
            let r = [],
                g = [],
                b = [];
            for (let d of this.diffs) {
                r.push(d.imgD.data[i * 4 + 0]);
                g.push(d.imgD.data[i * 4 + 1]);
                b.push(d.imgD.data[i * 4 + 2]);
            }
            difference = (Math.max(...r) - Math.min(...r)) + (Math.max(...g) - Math.min(...g)) + (Math.max(...b) - Math.min(...b));
            if (difference < this.lo) {
                px = 255;
            } else if (difference > this.hi) {
                px = 255 - difference;
            }

            r.sort();
            g.sort();
            b.sort();
            //if it's green
            if (g[0] > 200 && b[0] < 100 && r[0] < 100) {
                px = 255;
            }
            // if it's white across all?
            if (r[0] > 230) {
                px = 255;
            }

            if (r[this.diffs.length - 1] < 5) {

                px = 0;
            }

            last.imgD.data[i * 4 + 3] = px;
            this.pxtot += px;
        }

    } // end pixel loop
    //console.log("total and l", pxtot, l);
    this.pxtot = this.pxtot / l;
    console.log("pxtot", this.pxtot);
    this.dispCtx.putImageData(last.imgD, 0, 0);
    return Promise.resolve();
}

apparatus.cycle = async function() {
    for (let d of this.diffs) {
        await advanceFrame(this.interval);
        await copyPix(vid, d.ctx);
        d.imgD = d.ctx.getImageData(0, 0, d.canvas.width, d.canvas.width);

    }
    if (!document.hidden && this.pxtot && this.pxtot > 0 && this.pxtot < 130) {
        console.log("visible, uploading");
        try {
            await upImage(this.display);
        } catch (e) {
            console.log(e);
        }
    } else {
        console.log("out of frame");
    }
    if (whichVid === "hearing") {
        this.dispCtx.imageSmoothingEnabled = false

        for (let d of this.diffs) {
            var dets = await faceapi.detectAllFaces(d.canvas).withFaceLandmarks();

            //var dets = await faceapi.detectSingleFace(c2).withFaceLandmarks();

            if (dets.length) {
                console.log('hasface');
                var detectionsForSize = await faceapi.resizeResults(dets, {
                    width: this.display.width,
                    height: this.display.height
                });
                for (let face of dets) {
                    var oldStyle = this.dispCtx.fillStyle;
                    this.dispCtx.fillStyle = ("#16161d");
                    for (let mark of face.landmarks._positions) {
                        console.log(mark);

                        this.dispCtx.fillRect(mark.x, mark.y, 4, 4);
                    }
                    this.dispCtx.fillStyle = oldStyle;

                }

            }
        }
        await sleep(3000);

    } else {
        try {
            /*
            var result = await Tesseract.recognize(this.display);
            console.log("got result");
            //console.log(result);
            for (let b of result.blocks) {
                if (b.confidence > 32) {

                    console.log(b);
                    let x = b.bbox.x0;
                    let y = b.bbox.y0;
                    let w = b.bbox.x1 - b.bbox.x0;
                    let h = b.bbox.y1 - b.bbox.y0;
                    console.log(x, y, w, h);
                    this.dispCtx.fillStyle = "rgba(255,255,255,0)";
                    this.dispCtx.strokeStyle = "orange";
                    this.dispCtx.strokeWidth = 1;

                    this.dispCtx.strokeRect(x, y, w, h);
                    this.dispCtx.fillStyle = "orange";

                    this.dispCtx.fillText(b.confidence, x + w + 3, y)
                }
            }
            await sleep(5000);
            */
        } catch (err) {
            console.log(err);
            //break;
        }

    }
    await this.calculate();
    console.log("cycling...");
    let gap = this.gap + (50 - Math.floor(Math.random() * 100));
    console.log(gap);
    await sleep(gap);
    await this.cycle();

}

var cyc = async function() {
    await copyPix(vid, c1ctx);
    await advanceFrame(apparatus.interval);
    await copyPix(vid, c2ctx);
    await diff(c1ctx, c2ctx);
    if (whichVid === "hearing") {

        var dets = await faceapi.detectAllFaces(vid).withFaceLandmarks();

        //var dets = await faceapi.detectSingleFace(c2).withFaceLandmarks();

        if (dets.length) {
            alert("ddets");
            var detectionsForSize = await faceapi.resizeResults(dets, {
                width: c2.width,
                height: c2.height
            });
            for (let face of dets) {

                for (let mark of face.landmarks._positions) {
                    c3ctx.fillRect(mark.x, mark.y, 1.5, 1.5);
                }

            }

        }
    }
    await cyc();

}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

var setup = async function() {
    apparatus.metadata = await apparatus.pickFootage();
    if (whichVid === "hearing") {

        await faceapi.loadSsdMobilenetv1Model('./lib');
        await faceapi.nets.faceLandmark68Net.loadFromUri('./lib')
        vid.src = "trythis.mp4";
    } else if (whichVid === "glitch") {
        vid.src = "https://oversightmachin.es/baltimore/1D5003.webm";
    } else {
        //vid.src = "milbase.mp4";
        //vid.src = "https://oversightmachin.es/baltimore/1D4003.mp4";
        //vid.src = "https://oversightmachin.es/baltimore/1D3001Part1.webm";
        vid.src = "https://oversightmachin.es/baltimore/" + apparatus.metadata.filename.replace(".mp4", ".webm");
    }

    await videoReady();
    interval.setAttribute("max", (vid.duration * 2));
    if (startTime) {
        vid.currentTime = startTime;
    }
    await apparatus.setup();
    apparatus.recordId = apparatus.metadata.fillStyle.replace(".mp4", "");
    /*
    pixelMap = new Uint8ClampedArray(vid.videoWidth * vid.videoHeight);
    pixelMap.fill(255);
    console.log("ok to go");
    let h = vid.videoHeight;
    let w = vid.videoWidth;
    c1.height = h;
    c1.width = w;
    c2.height = h;
    c2.width = w;
    c3.height = h;
    c3.width = w;

    c3ctx.fillStyle = "rgba(1,24,6,0.8)";
    await sleep
    await cycle();
    */
};



var copyPix = async function(from, to) {
    to.drawImage(from, 0, 0);
    return Promise.resolve();
}



var videoReady = async function() {
    console.log("testing ready");
    return new Promise(function(resolve) {
        vid.addEventListener("canplay", function() {
            resolve();
        }, {
            once: true
        })
    });
}

Mousetrap.bind('x', function() {
    let slid = document.querySelector("#sliders");
    if (slid.style.display === "block") {
        slid.style.display = "none";
    } else {
        document.querySelector("#sliders").style.display = "block";
    }
});

Mousetrap.bind(']', function() {
    apparatus.gap = apparatus.gap + 125;
    console.log("bigger gap", apparatus.gap);

});

Mousetrap.bind('[', function() {
    if (apparatus.gap > 125) {
        apparatus.gap = apparatus.gap - 125;
    }
    console.log("smaller gap", apparatus.gap);

});

let backgrounds = [document.body.style.backgroundColor, "black", "white"];

Mousetrap.bind('B', function() {
    console.log("beee");
    let d = document.querySelector("#display");
    let bk = d.style.backgroundColor;
    let io = backgrounds.indexOf(bk);
    d.style.backgroundColor = backgrounds[(io + 1) % 3];
});



Mousetrap.bind('F', function() {
    let disp = document.querySelector("#display");
    disp.requestFullscreen();
});

Mousetrap.bind('esc', function() {

    document.exitFullscreen();
});


setup();

upImage = async function(image) {
    form = {
        "machine": "unburn",
        "mode": "d",
        "unburnCode": apparatus.title,
        "time": vid.currentTime,
        "data": {
            id: apparatus.title,
            interval: apparatus.interval,
            low: apparatus.lo,
            high: apparatus.hi,
            timepos: vid.currentTime
        },
        "image": image.toDataURL()
    };
    let md = JSON.stringify(apparatus.metadata);
    if (md.length > 20) {
        form.data.metadata = md;
    }
    var sData = JSON.stringify(form);
    console.log("sending image");
    try {
        let response = await fetch(imagerUrl, {
            method: "post",
            body: sData
        });
        response = await response.json();
        console.log("Request succeeded with JSON response", response);
    } catch (e) {
        console.log("fetch catch backup", e);
    }
};

document.querySelector("#display").addEventListener("dblclick", function() {
    document.querySelector("#display").requestFullscreen();
});

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}