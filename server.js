/* this is for local use only right now, easy tweaks to make it work on a vps or whatever tho */

//add ffmpeg stuff to this? 
//ffmpeg -i %05d.png -vcodec png z.mov
var san = require("sanitize-filename")
var https = require("http");
var fs = require("fs");
var outDir = "images/";

var server = https.createServer(async function (req, res) {

    //console.dir(req.param);

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, POST');
    res.setHeader('Access-Control-Allow-Headers', '*');

    if (req.method == 'POST') {
        console.log("POST");
        var body = '';
        req.on('data', function (data) {
            body += data.toString();
        });
        req.on('end', async function () {
            var inData = JSON.parse(body);



            if (!inData.id) {
                throw ("no id");
            } else {
                var id = san(inData.id);
            }

            if (inData.done) {
                //ffmpeg?
                //ffmpeg -f lavfi -i color=c=white:s=1280x720 -i vijtv-%05d.png -plays 0 -filter_complex "[0:v][1:v]overlay=shortest=1,format=yuv420p[out]" -map "[out]" fdsa.gif
		//ffmpeg -f lavfi -i color=AAAAAA -i nwjig.apng -filter_complex "[0][1]scale2ref[bg][gif];[bg]setsar=1[bg];[bg][gif]overlay=shortest=1" out.mp4

                console.log("Finished with " + id)
            }
            if (inData.image) {
                //get last image #
                let num = 0;
                let outsdir = outDir + id + "/";
                if (!fs.existsSync(outsdir)) {
                    fs.mkdirSync(outsdir)
                }
                let files = fs.readdirSync(outsdir);
                if (!files.length) {
                    console.log("no files");
                    num = 0;
                } else {
                    for (let file of files) {
                        if (file.includes(id)) {
                            let fn = parseInt(file.split("-")[1].split(".")[0], 10);
                            if (fn > num) {
                                num = fn;
                            }
                        }
                    }
                }
                num++;
                console.log(num);

                var out = outsdir + id + "-" + (num + "").padStart(5, "0") + ".png";
                console.log(out);
                var img = inData.image.replace(/^data:image\/png;base64,/, "");
                fs.writeFileSync(out, img, 'base64');
            } else {
                console.log("lol idk no image")
            }
            if (inData.data) {
                //console.log(inData);
                await processData(id, inData.time, inData.data);
            }
        });
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end('post received');
    }
    else {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end('what');
    }

});

port = 3000;
server.listen(port);
console.log('Listening at http://' + ':' + port);

async function processData(id, time, data) {
    let outsdir = outDir + id + "/";
    let dataFile = outsdir + "data.json";
    let ddata;
    if (!fs.existsSync(dataFile)) {
        console.log("no datafile yet");
        ddata = [];
        ddata.push({ id: id, time: time, data: data });
        console.log(ddata);
        fs.writeFileSync(dataFile, JSON.stringify(ddata));
        return true;
    } else {
        console.log("opening datafile");

        ddata = fs.readFileSync(dataFile);
        ddata = JSON.parse(ddata);
        last = ddata[ddata.length - 1];
        //oof, is this gonna work
        console.log(data);
        console.log(last);
        if (last.data.interval === data.interval && last.data.low === data.low && last.data.high === data.high) {
            return true;
        } else {
            console.log("updating datafile");

            ddata.push({ id: id, time: time, data: data });
            fs.writeFileSync(dataFile, JSON.stringify(ddata));
        }
    }
}

