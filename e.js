/*

ok this is going to iterate through frames of video
there are 3 canvases.
the video is offscreen
video frame gets loaded into c1,
video advances
video frame gets loaded into c2,
difference of each pixel's distance from value of prev frame is calculated

c3, fullscreen, shows corresponding pixels weighted over time -- so that over time pixels that don't change stay bright and ones that do go dark.
ideally weigh so that colors that start light stay light.  

also... a clamped array with 1 channel per pixel that iterates down based on how much it has changed toward zero.

//1d4001part3

*/

var $ = function(x) {
    return document.querySelector(x);
}

var vid = $("video");
var c1 = $("#c1");
var c1ctx = c1.getContext('2d');
var c2 = $("#c2");
var c2ctx = c2.getContext('2d');
var c3 = $("#c3");
var c3ctx = c3.getContext('2d');
var startTime = 0;
if (window.location.href.includes("localhost")) {
    var imagerUrl = "http://localhost:3000";
} else {
    var imagerUrl = "https://oversightmachin.es:3000/";
}
var parsedUrl = new URL(window.location.href);
var whichVid = parsedUrl.searchParams.get("form");
var footageid = parsedUrl.searchParams.get("id");


var advanceFrame = async function(time = 1 / 25) {

    return new Promise(function(resolve) {
        vid.addEventListener("seeked", resolve, {
            once: true
        });
        vid.currentTime = vid.currentTime + time;
    });
};

var pixelMap;

var diff = function(c1, c2) {
    let frame1 = c1.getImageData(0, 0, c1.canvas.width, c1.canvas.width);
    let frame2 = c2.getImageData(0, 0, c2.canvas.width, c2.canvas.width)



    let l = frame1.data.length / 4;

    for (let i = 0; i < l; i++) {
        let r1 = frame1.data[i * 4 + 0];
        let r2 = frame2.data[i * 4 + 0];
        let g1 = frame1.data[i * 4 + 1];
        let g2 = frame2.data[i * 4 + 1];
        let b1 = frame1.data[i * 4 + 2];
        let b2 = frame2.data[i * 4 + 2];
        let sum = r2 + g2 + b2;
        let dist = (Math.abs(r2 - r1) + Math.abs(g2 - g1) + Math.abs(b2 - b1));
        if (dist > 15 || sum < 7) {
            //pixelMap[i] = pixelMap[i] - (dist / 2);
            pixelMap[i] = 0;
        } else {
            pixelMap[i] = pixelMap[i] + 8;
        }
        frame1.data[i * 4 + 3] = pixelMap[i];
    }

    c3ctx.putImageData(frame1, 0, 0);
};

var cycle = async function() {
    await copyPix(vid, c1ctx);
    await advanceFrame(1 / 5);
    await copyPix(vid, c2ctx);
    await diff(c1ctx, c2ctx);
    if (whichVid === "hearing") {
        /*
        var dets = await faceapi.detectAllFaces(c2).withFaceLandmarks();
        //var dets = await faceapi.detectSingleFace(c2).withFaceLandmarks();

        if (dets.length) {
            var detectionsForSize = await faceapi.resizeResults(dets, { width: c2.width, height: c2.height });
            for (let face of dets) {

                for (let mark of face.landmarks._positions) {
                    c3ctx.fillRect(mark.x, mark.y, 1.5, 1.5);
                }

            }

            await sleep(600);
        }
        */
    } else {
        //let center = c2.getImageData(c2.canvas.width / 2, c2.canvas.height / 2, 1, 1);

        //document.body.style.background = "rgba(" + center.data[0] + "," + center.data[1] + "," + center.data[2] + ")";

    }
    await cycle();

}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function pickFootage() {
    let data = await fetch("https://oversightmachin.es/baltimore/data.json");
    data = await data.json();
    if (footageid) {
        for (let d of data) {
            if (d.filename.replace(".mp4", "") === footageid) {
                return d;
            }

        }
    }
    return data[Math.floor(Math.random() * data.length)];

}

async function the_title(inc) {
    let title = document.querySelector("#title");
    document.querySelector("title").textContent = "u̶n̶burning " + inc;
    title.textContent = "u̶n̶burning " + inc;
    title.style.display = "block";
    await sleep(4242);
    title.style.display = "none";
    return Promise.resolve();
}

async function the_JSON(inc) {
    let jsholder = document.querySelector("#json");

    jsholder.textContent = JSON.stringify(inc);
    jsholder.style.display = "block";

    await sleep(9001);
    jsholder.style.display = "none";
    return Promise.resolve();
}

var setup = async function() {
    //await faceapi.loadSsdMobilenetv1Model('./lib');
    //await faceapi.nets.faceLandmark68Net.loadFromUri('./lib')
    let metadata = await pickFootage();
    await the_title(metadata.filename.replace(".mp4", ""));
    await the_JSON(metadata.metadata);
    if (whichVid === "hearing") {

        vid.src = "https://oversightmachin.es/oversee/media/video/170511_1000.mp4";
        startTime = 1260;
    } else if (whichVid === "glitch") {
        vid.src = "https://oversightmachin.es/baltimore/1D5003.webm";
    } else {
        vid.src = "https://oversightmachin.es/baltimore/" + metadata.filename.replace(".mp4", ".webm") || "https://oversightmachin.es/baltimore/1D3001Part1.webm";
    }

    await videoReady();
    if (startTime) {
        vid.currentTime = startTime;
    }
    pixelMap = new Uint8ClampedArray(vid.videoWidth * vid.videoHeight);
    pixelMap.fill(255);
    console.log("ok to go");
    let h = vid.videoHeight;
    let w = vid.videoWidth;
    c1.height = h;
    c1.width = w;
    c2.height = h;
    c2.width = w;
    c3.height = h;
    c3.width = w;

    c3ctx.fillStyle = "rgba(1,24,6,0.8)";
    await sleep
    await cycle();
};



var copyPix = async function(from, to) {
    to.drawImage(from, 0, 0);
    return Promise.resolve();
}

vid.addEventListener("ended", function() {
    location.reload();
});

var videoReady = async function() {
    console.log("testing ready");
    return new Promise(function(resolve) {
        vid.addEventListener("canplay", function() {
            resolve();
        }, {
            once: true
        })
    });
}



setup();