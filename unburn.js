var vid = document.querySelector('video');
vid.src = "https://oversightmachin.es/baltimore/1D3001Part1.webm";
vid.setAttribute("crossorigin", ""); //because CORS
var c1 = document.querySelector('#c1');
var c1c = c1.getContext('2d');
var arr = document.querySelector("#arrow");

var c2 = document.querySelector('#c2');
var c2c = c2.getContext('2d');

var data = document.querySelector('#data');
var ol = document.querySelector('#outline');
var imgc = document.querySelector('#imgcont');
var imgc = document.querySelector('#imgcont');
var arrtar = document.querySelector('#arrowtar');
var arrpiece = document.querySelector('#arrpiece');
var lc = document.querySelector('#layercanv');
var lcc = lc.getContext('2d');
lcc.fillStyle = "black";
var vw, vh;

vid.addEventListener('canplaythrough', function () {
    vid.removeEventListener('canplaythrough', arguments.callee);
    setup();
});


var Zone = function (options) {
    //t (title),x,y,w,h, ... type?
    options = options || {};
    for (var o in options) {
        this[o] = options[o];
    }
};

var outline = function (zone) {
    ol.style.left = pctw(zone.x);
    ol.style.top = pcth(zone.y);
    ol.style.width = pctw(zone.w);
    ol.style.height = pcth(zone.h);
    //    return zone;
};

var zones = [];

function pctw(input) {
    return input / vw * 100 + "%";
}

function pcth(input) {
    return input / vh * 100 + "%";
}


mZone = function (x, y, w, h, options) {

}

dataz = {
    x: 1101,
    y: 580,
    w: 170,
    h: 128
}

imgz = {
    x: 758,
    y: 652,
    w: 337,
    h: 55
};

var getZones = async function (url) {
    var result = await fetch('1d3001part1.json');
    let data = await result.json();
    for (zone of data) {
        zones.push(new Zone(zone));
    }
    return true;
};

console.log(zones);

var arrow = {
    x: 19,
    y: 102,
    w: 95,
    h: 80
};

var arrowz = {
    x: 863,
    y: 5,
    w: 101,
    h: 53
}

var record = {};

var logz = {
    x: 18,
    y: 563,
    w: 298,
    h: 139
};

var arrowtar = {
    x: 1188,
    y: 8,
    w: 86,
    h: 48
}

async function setup() {

    await getZones();


    for (let zone of zones) {
        record[zone.title] = [];
    }
    vh = vid.videoHeight;
    vw = vid.videoWidth;

    lc.height = vh;
    lc.width = vw;
    iterateVid(0.3);
    data.style.left = pctw(dataz.x);
    data.style.top = pcth(dataz.y);
    data.style.width = pctw(dataz.w);
    data.style.height = pcth(dataz.h);
    imgc.style.left = pctw(imgz.x);
    imgc.style.top = pcth(imgz.y);
    imgc.style.width = pctw(imgz.w);
    imgc.style.height = pcth(imgz.h);

    log.style.left = pctw(logz.x);
    log.style.top = pcth(logz.y);
    log.style.width = pctw(logz.w);
    log.style.height = pcth(logz.h);
    c2.width = arrow.w;
    c2.height = arrow.h;

    arr.style.left = pctw(arrowz.x);
    arr.style.top = pcth(arrowz.y);
    arr.style.width = pctw(arrowz.w);
    arr.style.height = pcth(arrowz.h);

    arrtar.style.left = pctw(arrowtar.x);
    arrtar.style.top = pcth(arrowtar.y);
    arrtar.style.width = pctw(arrowtar.w);
    arrtar.style.height = pcth(arrowtar.h);


}

var rnd = function (top) {
    return (Math.random() * top);

}

function sleep(until) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, (until));
    });
}

function scrLog(msg) {
    var div = document.createElement("div");
    div.textContent = msg;
    log.appendChild(div);
    log.scrollTop = log.scrollHeight;

}

var iterateVid = async function (interval) {
    lcc.fillRect(0, 0, lcc.width, lcc.height);
    console.log("iterating");
    await seeked(interval);
    c2c.drawImage(vid, arrow.x, arrow.y, arrow.w, arrow.h, 0, 0, arrow.w, arrow.h);
    lcc.drawImage(vid, arrow.x, arrow.y, arrow.w, arrow.h, arrow.x, arrow.y, arrow.w, arrow.h);

    checkArrow(c2);

    for (let zone of zones) {
        console.log("trying zone");
        c1.width = zone.w;
        c1.height = zone.h;
        c1c.drawImage(vid, zone.x, zone.y, zone.w, zone.h, 0, 0, zone.w, zone.h);
        lcc.drawImage(vid, zone.x, zone.y, zone.w, zone.h, zone.x, zone.y, zone.w, zone.h);
        outline(zone);
        var result;
        try {
            var result = await Tesseract.recognize(c1);
            console.log("got result");
            console.dir(result);
        } catch (err) {
            console.log(err);
            break;
        }

        /* .progress(async function (message) {
            var line = document.createElement('div');
            line.textContent = JSON.stringify(message);
            data.appendChild(line);
            data.scrollTop = data.scrollHeight;
        });*/
        var line = document.createElement('div');
        if (record[zone.title].length && record[zone.title][record[zone.title].length - 1].value === result.text) {
            console.log("match");
        } else {
            record[zone.title].push({
                time: vid.currentTime,
                value: result.text,
                confidence: result.confidence
            });
            scrLog("New value " + result.text + "(" + result.confidence + ") " + "at " + vid.currentTime);
        }
        line.textContent = result.text;
        if (result.confidence < 60) {
            line.classList.add("suspect");
        }
        data.appendChild(line);
        data.scrollTop = data.scrollHeight;

    }
    c2c.clearRect(0, 0, c2.width, c2.height);
    console.log("and again");
    iterateVid(interval)

}

var seeked = async function (interval) {
    vid.currentTime = vid.currentTime + interval;

    return new Promise(function (resolve, reject) {
        vid.addEventListener('seeked', function () {
            vid.removeEventListener('seeked', arguments.callee);
            return resolve();

        });
    });
};

var checkArrow = async function (c2) {
    var counter = 0;
    var can2 = document.querySelector('#arrpiece');
    var arrctx = can2.getContext('2d');
    //rows
    var div = 3;
    var unitw = c2.width / div;
    var unith = c2.height / div;
    can2.width = unitw;
    can2.height = unith;
    var winner;
    var totals = [];
    var highest = 0;
    for (let y = 0; y < div; y++) {

        for (let x = 0; x < div; x++) {

            counter++;
            arrctx.drawImage(c2, unitw * x, unith * y, unitw, unith, 0, 0, unitw, unith);
            var id = arrctx.getImageData(0, 0, unitw, unith);
            var total = 0;
            for (let d of id.data) {
                if (d < 200) {
                    d = 0;
                }
                total += d;
            }
            arrctx.putImageData(id, 0, 0);
            console.log(total);
            if (total > highest) {
                console.log(total, counter);
                highest = total;
                winner = counter;
            }
            totals.push(total);


        }

    }
    var degree;
    if (winner === 1) {
        degree = 315;
    } else if (winner === 2) {
        degree = 0;
    } else if (winner === 3) {
        degree = 45;
    } else if (winner === 4) {
        degree = 270;
    } else if (winner === 6) {
        degree = 90;
    } else if (winner === 7) {
        degree = 225;
    } else if (winner === 8) {
        degree = 180;
    } else if (winner === 9) {
        degree = 135;
    } else {
        degree = "??"
    }
    if (!winner) {
        arrtar.style.color = "orange";
    } else {
        arrtar.style.color = "papayawhip";
        let txt = degree + '\u00b0';
        if (arrtar.textContent !== txt) {

            arrtar.textContent = txt;
            scrLog("New value: " + degree);
        }
    }
}
///upload data

uploadData = function (blob, time) {

    form = {
        data: data,
        timestamp: time
    };

    return new Promise(function (resolve) {
        var sData = JSON.stringify(form);
        console.log("sending image");
        try {
            fetch("localhost:3000", {
                method: "post",
                body: sData
            }).then(json).then(function (data) {
                console.log("Request succeeded with JSON response", data);

                return resolve();
            }).catch(function (error) {
                console.log("Request failed", error);
            });
        } catch (e) {
            console.log("fetch catch backup", e);
        }

    });
};



////fullscreen
document.addEventListener("keydown", function (e) {
    if (e.keyCode == 13) {
        toggleFullScreen();
    }
}, false);

function toggleFullScreen() {
    if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen();
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }
}
